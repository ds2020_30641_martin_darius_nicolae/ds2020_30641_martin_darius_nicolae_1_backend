package ro.tuc.ds2020.controllers;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.Link;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.PatientDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.services.CaregiverService;
import ro.tuc.ds2020.services.PatientService;


import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

@RestController
@CrossOrigin
@RequestMapping(value = "/caregiver")
public class CaregiverController {

    private final CaregiverService caregiverService;
    private final PatientService patientService;

    @Autowired
    public CaregiverController(CaregiverService caregiverService, PatientService patientService) {

        this.caregiverService = caregiverService;
        this.patientService = patientService;
    }

    @GetMapping("/allCaregivers")
    public ResponseEntity<List<CaregiverDTO>> getCaregiver() {
        List<CaregiverDTO> dtos = caregiverService.findCaregivers();
        for (CaregiverDTO dto : dtos) {
            Link caregiverLink = linkTo(methodOn(CaregiverController.class)
                    .getCaregiver(dto.getId_caregiver())).withRel("patientDetails");
            dto.add(caregiverLink);
        }
        return new ResponseEntity<>(dtos, HttpStatus.OK);
    }

    @PostMapping("/insert")
    public ResponseEntity<UUID> insertCaregiver(@Valid @RequestBody CaregiverDTO caregiverDTO) {
        UUID caregiverID = caregiverService.insert(caregiverDTO);
        return new ResponseEntity<>(caregiverID, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<CaregiverDTO> getCaregiver(@PathVariable("id") UUID caregiverId) {
        CaregiverDTO dto = caregiverService.findCaregiverById(caregiverId);
        return new ResponseEntity<>(dto, HttpStatus.OK);
    }


    @DeleteMapping("/delete/{name}")
    public ResponseEntity<UUID> deleteCaregiver(@PathVariable("name") String caregiverName) {
        List<PatientDTO> patients=patientService.findPatients();
        for(PatientDTO patient: patients){
            if(patient.getCaregiver()!=null){
                if(patient.getCaregiver().getName().equals(caregiverName)) {
                    patient.setCaregiver(null);
                    patientService.update(patient);
                }
            }
        }
        UUID id=caregiverService.delete(caregiverName);
        return  new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PutMapping("/update/{name}")
    public ResponseEntity<UUID> updateCaregiver(@PathVariable("name") String caregiverName,@Valid @RequestBody CaregiverDTO updatedCaregiver) {
        CaregiverDTO caregiverDTO=caregiverService.findCaregiverByName(caregiverName);
        caregiverDTO=caregiverService.findCaregiverById(caregiverDTO.getId_caregiver());
        caregiverDTO.setAddress(updatedCaregiver.getAddress());
        caregiverDTO.setName(updatedCaregiver.getName());
        caregiverDTO.setGender(updatedCaregiver.getGender());
        caregiverDTO.setBirthDate(updatedCaregiver.getBirthDate());
        caregiverDTO.setEmail(updatedCaregiver.getEmail());
        caregiverDTO.setPassword(updatedCaregiver.getPassword());
        UUID id=caregiverService.update(caregiverDTO);
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @PostMapping("/PacientToCaregiver")
    public ResponseEntity<UUID> pacientToCaregiver(@RequestParam(value="numePacient")String numePacient,@RequestParam(value="numeCaregiver")String numeCaregiver){
        CaregiverDTO caregiverDTO=caregiverService.findCaregiverByName(numeCaregiver);
        System.out.println(caregiverDTO.getId_caregiver());
        PatientDTO patientDTO=patientService.findPatientByName(numePacient);

        caregiverDTO=caregiverService.findCaregiverById(caregiverDTO.getId_caregiver());
        patientDTO=patientService.findPatientById(patientDTO.getId());

        patientDTO.setCaregiver(caregiverDTO);
        UUID id=patientService.update(patientDTO);
        
        return new ResponseEntity<>(id, HttpStatus.OK);
    }

    @GetMapping("/AllPatients/{name}")
    public List<PatientDTO> allPatients(@PathVariable("name") String caregiverName){
        CaregiverDTO caregiver=caregiverService.findCaregiverByName(caregiverName);
        List<PatientDTO> patients=patientService.findPatients();
        List<PatientDTO> patients2=new ArrayList<PatientDTO>();
        for(int i=0;i<patients.size();i++) {
            PatientDTO patient=patients.get(i);
            if (patient.getCaregiver() != null) {
                if (patient.getCaregiver().getId_caregiver().equals(caregiver.getId_caregiver())) {
                    patients2.add(patient);
                }
            }
        }
        return patients2;
    }



}
