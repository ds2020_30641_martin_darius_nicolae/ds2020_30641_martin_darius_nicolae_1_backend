package ro.tuc.ds2020.dtos.builders;

import ro.tuc.ds2020.dtos.CaregiverDTO;
import ro.tuc.ds2020.dtos.MedicationDTO;
import ro.tuc.ds2020.entities.Caregiver;
import ro.tuc.ds2020.entities.Medication;

public class MedicationBuilder {

    private MedicationBuilder() {
    }

    public static MedicationDTO toMedicationDTO(Medication medication) {
        return new MedicationDTO(medication.getId_medication(),medication.getName(),medication.getSideEffects(),medication.getDosage());
    }


    public static Medication toEntity(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getName(),medicationDTO.getSideEffects(),medicationDTO.getDosage());
    }

    public static Medication transform(MedicationDTO medicationDTO) {
        return new Medication(medicationDTO.getId_medication(),medicationDTO.getName(),medicationDTO.getSideEffects(),medicationDTO.getDosage());
    }
}
